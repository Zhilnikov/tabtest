//
//  QuestionsViewModelTests.swift
//  TABTestTests
//

import XCTest
@testable import TABTest

class QuestionsViewModelTests: XCTestCase {

  private var sut: QuestionsViewModel!
  private var viewController: QuestionsViewControllerMock!
  private let updatableScoreViewModel = UpdatableScoreViewModelMock()
  
  override func setUp() {
    viewController = QuestionsViewControllerMock()
    
    sut = QuestionsViewModel(viewController: viewController,
                             updatableScoreViewModel: updatableScoreViewModel)
    sut.loadQuestions()
  }
  
  func testTitles() {
    for i in 1...10 {
      XCTAssertEqual(sut.title, "Question \(i)")
      sut.nextQuestion()
    }
  }
  
  func testQuestions() {
    XCTAssertEqual(sut.question, "Extensions must not contain stored properties?")
    sut.nextQuestion()
    
    XCTAssertEqual(sut.question, "A struct created as a var can have its variables changed?")
    sut.nextQuestion()
    
    XCTAssertEqual(sut.question, "Class is value type?")
    sut.nextQuestion()
    
    XCTAssertEqual(sut.question, "Mutating functions can be used in classes?")
    sut.nextQuestion()
    
    XCTAssertEqual(sut.question, "Enumerations in Swift can conform to protocols?")
  }
  
  func testShowNextQuestion() {
    XCTAssertTrue(viewController.isShowNextQuestionCalled)
    viewController.isShowNextQuestionCalled = false
    
    sut.nextQuestion()
    XCTAssertTrue(viewController.isShowNextQuestionCalled)
  }
  
  func testVerifyRightAnswers() {
    sut.verify(answer: true)
    XCTAssertEqual(viewController.answer, "Your answer is right")
    viewController.answer = ""
    
    sut.nextQuestion()
    sut.verify(answer: true)
    XCTAssertEqual(viewController.answer, "Your answer is right")
    viewController.answer = ""
    
    sut.nextQuestion()
    sut.verify(answer: false)
    XCTAssertEqual(viewController.answer, "Your answer is right")
    viewController.answer = ""
    
    sut.nextQuestion()
    sut.verify(answer: false)
    XCTAssertEqual(viewController.answer, "Your answer is right")
    viewController.answer = ""
    
    sut.nextQuestion()
    sut.verify(answer: true)
    XCTAssertEqual(viewController.answer, "Your answer is right")
  }
  
  func testVerifyWrongAnswers() {
    sut.verify(answer: false)
    XCTAssertEqual(viewController.answer, "Your answer is wrong")
    viewController.answer = ""
    
    sut.nextQuestion()
    sut.verify(answer: false)
    XCTAssertEqual(viewController.answer, "Your answer is wrong")
    viewController.answer = ""
    
    sut.nextQuestion()
    sut.verify(answer: true)
    XCTAssertEqual(viewController.answer, "Your answer is wrong")
    viewController.answer = ""
    
    sut.nextQuestion()
    sut.verify(answer: true)
    XCTAssertEqual(viewController.answer, "Your answer is wrong")
    viewController.answer = ""
    
    sut.nextQuestion()
    sut.verify(answer: false)
    XCTAssertEqual(viewController.answer, "Your answer is wrong")
  }
  
  func testAnswerButtonsViewEnable() {
    sut.nextQuestion()
    XCTAssertTrue(viewController.isAnswerButtonsEnabled)
  }
  
  func testAnswerButtonsViewDisable() {
    sut.verify(answer: true)
    XCTAssertFalse(viewController.isAnswerButtonsEnabled)
  }
  
  func testDismiss() {
    sut.done()
    XCTAssertTrue(viewController.isDismissCalled)
  }
  
  func testAnswerViewHidden() {
    sut.nextQuestion()
    XCTAssertTrue(viewController.isAnswerViewHidden)
  }
  
  func testAnswerViewNotHidden() {
    sut.verify(answer: false)
    XCTAssertFalse(viewController.isAnswerViewHidden)
  }
  
  func testRightAnswerScore() {
    // Right answer
    sut.verify(answer: true)
    sut.nextQuestion()
    
    // Wrong answer
    sut.verify(answer: false)
    sut.nextQuestion()
    
    // Right answer
    sut.verify(answer: false)
    sut.nextQuestion()
    
    // Right answer
    sut.verify(answer: false)
    sut.nextQuestion()
    
    // Wrong answer
    sut.verify(answer: false)
    
    sut.done()
    XCTAssertEqual(updatableScoreViewModel.score, 3)
  }
}

class QuestionsViewControllerMock: QuestionViewControllerProtocol {
  
  var isShowNextQuestionCalled = false
  var answer = ""
  var isAnswerViewHidden = true
  var isAnswerButtonsEnabled = true
  private(set) var isDismissCalled = false
  
  func didFailLoadQuestions(_ description: String) {
  }
  
  func showNextQuestion() {
    isShowNextQuestionCalled = true
  }
  
  func showAnswer(_ answer: String) {
    self.answer = answer
  }
  
  func hideAnswerView(_ isHidden: Bool) {
    isAnswerViewHidden = isHidden
  }
  
  func enableAnswerButtons(_ isEnabled: Bool) {
    isAnswerButtonsEnabled = isEnabled
  }
  
  func dismiss() {
    isDismissCalled = true
  }
}

class UpdatableScoreViewModelMock: UpdatableScore {
  
  private(set) var score = 0
  
  func update(score: Int) {
    self.score = score
  }
}
