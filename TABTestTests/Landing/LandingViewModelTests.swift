//
//  LandingViewModelTests.swift
//  TABTestTests
//

import XCTest
@testable import TABTest

class LandingViewModelTests: XCTestCase {

  private var sut: LandingViewModel!
  private var viewController: LandingViewControllerMock!
  
  override func setUp() {
    viewController = LandingViewControllerMock()
    sut = LandingViewModel()
    sut.delegate = viewController
  }
  
  func testPlayAction() {
    sut.play()
    XCTAssertTrue(viewController.isPlayActionCalled)
  }
  
  func testUpdateScore() {
    sut.update(score: 1)
    XCTAssertTrue(viewController.isNeedsUpdateScoreCalled)
  }
  
  func testScore() {
    let score = 4
    sut.update(score: score)
    XCTAssertEqual(sut.score, score)
  }
}

class LandingViewControllerMock: LandingViewControllerProtocol {
  
  private(set) var isPlayActionCalled = false
  private(set) var isNeedsUpdateScoreCalled = false
  
  func playAction() {
    isPlayActionCalled = true
  }
  
  func needsUpdateScore() {
    isNeedsUpdateScoreCalled = true
  }
}
