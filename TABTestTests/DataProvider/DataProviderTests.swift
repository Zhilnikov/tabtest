//
//  DataProviderTests.swift
//  TABTestTests
//

import XCTest
@testable import TABTest

class DataProviderTests: XCTestCase {
  
  private var sut: QuizDataProvider!
  
  func testQuestionsCount() {
    do {
      sut = DataProvider()
      let questions = try sut.questions()
      XCTAssertEqual(questions.count, 5)
    } catch {
      XCTFail()
    }
  }
  
  func testEmptyQuestionsThrowError() {
    sut = EmptyDataProvider()
    XCTAssertThrowsError(try sut.questions()) { error in
      XCTAssertEqual(error as? JSONError, .emptyData)
    }
  }
  
  func testUnknownQuestionsThrowError() {
    sut = UnknownDataProvider()
    XCTAssertThrowsError(try sut.questions()) { error in
      XCTAssertEqual(error as? JSONError, .invalidURL)
    }
  }
}

class EmptyDataProvider: QuizDataProvider {
  
  func questions() throws -> [QuestionAnswer] {
    guard let url = Bundle(for: EmptyDataProvider.self).url(forResource: "EmptyQuiz", withExtension: "json") else {
      throw JSONError.invalidURL
    }
    
    do {
      let data = try Data(contentsOf: url)
      
      let decoder = JSONDecoder()
      let quiz = try decoder.decode(Quiz.self, from: data)
      
      if quiz.questions.isEmpty {
        throw JSONError.emptyData
      }
      
      return quiz.questions
    } catch {
      throw error
    }
  }
}

class UnknownDataProvider: QuizDataProvider {
  
  func questions() throws -> [QuestionAnswer] {
    guard let url = Bundle(for: UnknownDataProvider.self).url(forResource: "", withExtension: "") else {
      throw JSONError.invalidURL
    }
    
    do {
      let data = try Data(contentsOf: url)
      
      let decoder = JSONDecoder()
      let quiz = try decoder.decode(Quiz.self, from: data)
      
      if quiz.questions.isEmpty {
        throw JSONError.emptyData
      }
      
      return quiz.questions
    } catch {
      throw error
    }
  }
}
