//
//  QuestionsViewModel.swift
//  TABTest
//

import Foundation

protocol QuestionViewControllerProtocol: class {
  func didFailLoadQuestions(_ description: String)
  func showNextQuestion()
  func showAnswer(_ answer: String)
  func hideAnswerView(_ isHidden: Bool)
  func enableAnswerButtons(_ isEnabled: Bool)
  func dismiss()
}

final class QuestionsViewModel {
  
  var title: String {
    return "Question \(questionNumber + 1)"
  }
  
  var question: String {
    return questions[questionIndex].question
  }
  
  private var questionIndex: Int {
    return questionNumber % questions.count
  }
  
  private var correctAnswers = 0
  private var questionNumber = 0
  private var questions: [QuestionAnswer] = []
  
  private unowned let viewController: QuestionViewControllerProtocol
  private unowned let updatableScoreViewModel: UpdatableScore
  
  init(viewController: QuestionViewControllerProtocol,
       updatableScoreViewModel: UpdatableScore,
       correctAnswers: Int = 0) {
    self.viewController = viewController
    self.updatableScoreViewModel = updatableScoreViewModel
    self.correctAnswers = correctAnswers
  }
  
  func loadQuestions() {
    do {
      let dataProvider = DataProvider()
      questions = try dataProvider.questions()
      prepareNextQuestion()
    } catch {
      viewController.didFailLoadQuestions(error.localizedDescription)
    }
  }
  
  func verify(answer: Bool) {
    let answerText = questions[questionIndex].answer == answer ? "right" : "wrong"
    
    if questions[questionIndex].answer == answer {
      correctAnswers += 1
    }
    
    viewController.showAnswer("Your answer is \(answerText)")
    viewController.enableAnswerButtons(false)
    viewController.hideAnswerView(false)
  }
  
  func nextQuestion() {
    questionNumber += 1
    prepareNextQuestion()
  }
  
  func done() {
    updatableScoreViewModel.update(score: correctAnswers)
    viewController.dismiss()
  }
  
  private func prepareNextQuestion() {
    viewController.hideAnswerView(true)
    viewController.enableAnswerButtons(true)
    viewController.showNextQuestion()
  }
}
