//
//  QuestionsViewController.swift
//  TABTest
//

import UIKit

final class QuestionsViewController: UIViewController {
  
  var viewModel: QuestionsViewModel?
  
  private let questionLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  private let answerButtonsView = AnswerButtonsView(frame: .zero)
  private let answerView = AnswerView(frame: .zero)
  
  override func loadView() {
    let view = UIView()
    self.view = view
    
    view.addSubview(questionLabel)
    view.addSubview(answerButtonsView)
    view.addSubview(answerView)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .white
    view.layoutMargins = UIEdgeInsets(top: 80, left: 40, bottom: 20, right: 40)
    
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done",
                                                        style: .done,
                                                        target: self,
                                                        action: #selector(doneButtonTapped))
    setConstraints()
    
    answerButtonsView.buttonAction = { [weak self] answer in
      self?.viewModel?.verify(answer: answer)
    }
    
    answerView.nextQuestionAction = { [weak self] in
      self?.viewModel?.nextQuestion()
    }
    
    viewModel?.loadQuestions()
  }
  
  private func setConstraints() {
    questionLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
    questionLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor).isActive = true
    questionLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor).isActive = true
    
    answerButtonsView.topAnchor.constraint(equalTo: questionLabel.bottomAnchor, constant: 40).isActive = true
    answerButtonsView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor).isActive = true
    answerButtonsView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor).isActive = true
    
    answerView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor).isActive = true
    answerView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor).isActive = true
    answerView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
  }
  
  @objc private func doneButtonTapped(_ sender: UIBarButtonItem) {
    viewModel?.done()
  }
}

extension QuestionsViewController: QuestionViewControllerProtocol {
  
  func didFailLoadQuestions(_ description: String) {
    let okAction = UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
      self?.dismiss(animated: true)
    })
    
    let alert = UIAlertController(title: "Error", message: description, preferredStyle: .alert)
    alert.addAction(okAction)
    present(alert, animated: true)
  }
  
  func showNextQuestion() {
    navigationItem.title = viewModel?.title
    questionLabel.text = viewModel?.question
  }
  
  func showAnswer(_ answer: String) {
    answerView.answer = answer
  }
  
  func hideAnswerView(_ isHidden: Bool) {
    answerView.isHidden = isHidden
  }
  
  func enableAnswerButtons(_ isEnabled: Bool) {
    answerButtonsView.isUserInteractionEnabled = isEnabled
  }
  
  func dismiss() {
    dismiss(animated: true)
  }
}
