//
//  QuestionAnswer.swift
//  TABTest
//

struct QuestionAnswer: Decodable {
  
  let question: String
  let answer: Bool
  
  private enum CodingKeys: String, CodingKey {
    case question
    case answer
  }
  
  init(from decoder: Decoder) throws {
    do {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      self.question = try container.decode(String.self, forKey: .question)
      self.answer = try container.decode(Bool.self, forKey: .answer)
    } catch {
      throw JSONError.invalidData
    }
  }
}
