//
//  AnswerButtonsView.swift
//  TABTest
//

import UIKit

final class AnswerButtonsView: UIStackView {
  
  var buttonAction: ((Bool) -> Void)?
  
  private enum ButtonTitle: String {
    case True
    case False
  }
  
  private let trueButton: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle(ButtonTitle.True.rawValue, for: .normal)
    button.contentEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 20)
    button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    return button
  }()
  
  private let falseButton: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle(ButtonTitle.False.rawValue, for: .normal)
    button.contentEdgeInsets = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 0)
    button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    return button
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    configure()
    translatesAutoresizingMaskIntoConstraints = false
  }
  
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configure() {
    addArrangedSubview(trueButton)
    addArrangedSubview(falseButton)
    
    axis = .horizontal
    distribution = .equalSpacing
  }
  
  @objc private func buttonTapped(_ sender: UIButton) {
    if let title = sender.title(for: .normal), let buttonTitle = ButtonTitle(rawValue: title) {
      buttonAction?(buttonTitle == .True)
    }
  }
}
