//
//  AnswerView.swift
//  TABTest
//

import UIKit

final class AnswerView: UIStackView {
  
  var nextQuestionAction: (() -> Void)?
  var answer: String? {
    didSet {
      answerLabel.text = answer
    }
  }
  
  private let answerLabel = UILabel(frame: .zero)
  
  private let nextQuestionButton: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("Next Question", for: .normal)
    button.contentEdgeInsets = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
    button.addTarget(self, action: #selector(nextQuestionButtonTapped), for: .touchUpInside)
    return button
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    configure()
  }
  
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configure() {
    translatesAutoresizingMaskIntoConstraints = false
    
    addArrangedSubview(answerLabel)
    addArrangedSubview(nextQuestionButton)
    
    axis = .vertical
    spacing = 10
    alignment = .center
  }
  
  @objc private func nextQuestionButtonTapped() {
    nextQuestionAction?()
  }
}
