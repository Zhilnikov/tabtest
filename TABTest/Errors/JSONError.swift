//
//  JSONError.swift
//  TABTest
//

import Foundation

enum JSONError: Error {
  case invalidData
  case invalidURL
  case emptyData
}

extension JSONError: LocalizedError {
  
  var errorDescription: String? {
    
    switch self {
    case .invalidData:
      return NSLocalizedString("JSON parsing", comment: "")
      
    case .invalidURL:
      return NSLocalizedString("Invalid URL", comment: "")
      
    case .emptyData:
      return NSLocalizedString("Empty data", comment: "")
    }
  }
}
