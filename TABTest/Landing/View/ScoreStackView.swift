//
//  ScoreStackView.swift
//  TABTest
//

import UIKit

final class ScoreStackView: UIStackView {
  
  var score: Int = 0 {
    didSet {
      scoreLabel.text = String(score)
    }
  }
  
  private let titleLabel: UILabel = {
    let label = UILabel(frame: .zero)
    label.text = "Score"
    label.font = UIFont.preferredFont(forTextStyle: .headline)
    return label
  }()
  
  private let scoreLabel = UILabel(frame: .zero)
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    configure()
  }
  
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func update(score: Int) {
    scoreLabel.text = String(score)
  }
  
  private func configure() {
    translatesAutoresizingMaskIntoConstraints = false
    
    addArrangedSubview(titleLabel)
    addArrangedSubview(scoreLabel)
    
    axis = .vertical
    alignment = .center
    spacing = 80
  }
}
