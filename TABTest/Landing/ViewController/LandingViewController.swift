//
//  LandingViewController.swift
//  TABTest
//

import UIKit

final class LandingViewController: UIViewController {
  
  private let scoreStackView = ScoreStackView()
  private let playButton: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("Play", for: .normal)
    button.contentEdgeInsets = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.addTarget(self, action: #selector(playButtonTapped), for: .touchUpInside)
    return button
  }()
  
  private let viewModel = LandingViewModel()
  
  override func loadView() {
    let view = UIView()
    self.view = view
    
    view.addSubview(scoreStackView)
    view.addSubview(playButton)
    setConstraints()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .white
    view.layoutMargins = UIEdgeInsets(top: 80, left: 0, bottom: 20, right: 0)
    viewModel.delegate = self
    needsUpdateScore()
  }
  
  private func setConstraints() {
    scoreStackView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
    scoreStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    
    playButton.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
    playButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
  }
  
  @objc private func playButtonTapped(_ sender: UIButton) {
    viewModel.play()
  }
}

extension LandingViewController: LandingViewControllerProtocol {
  
  func playAction() {
    let questionsViewController = QuestionsViewController()
    let questionsViewModel = QuestionsViewModel(viewController: questionsViewController,
                                                updatableScoreViewModel: viewModel)
    questionsViewController.viewModel = questionsViewModel
    let navigationController = UINavigationController(rootViewController: questionsViewController)
    present(navigationController, animated: true)
  }
  
  func needsUpdateScore() {
    scoreStackView.score = viewModel.score
  }
}
