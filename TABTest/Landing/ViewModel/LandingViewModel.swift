//
//  LandingViewModel.swift
//  TABTest
//

import Foundation

protocol LandingViewControllerProtocol: class {
  func playAction()
  func needsUpdateScore()
}

protocol UpdatableScore: class {
  func update(score: Int)
}

final class LandingViewModel {
  
  weak var delegate: LandingViewControllerProtocol?
  
  private(set) var score = 0
  
  func play() {
    delegate?.playAction()
    score = 0
  }
}

extension LandingViewModel: UpdatableScore {
  
  func update(score: Int) {
    self.score = score
    delegate?.needsUpdateScore()
  }
}
