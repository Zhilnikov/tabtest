//
//  Question.swift
//  TABTest
//

struct Quiz: Decodable {
  
  let domain: String
  let questions: [QuestionAnswer]
  
  private enum CodingKeys: String, CodingKey {
    case domain
    case questions
  }
  
  init(from decoder: Decoder) throws {
    do {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      self.domain = try container.decode(String.self, forKey: .domain)
      self.questions = try container.decode([QuestionAnswer].self, forKey: .questions)
    } catch {
      throw JSONError.invalidData
    }
  }
}
