//
//  DataProvider.swift
//  TABTest
//

import Foundation

protocol QuizDataProvider {
  func questions() throws -> [QuestionAnswer]
}

final class DataProvider: QuizDataProvider {
  
  func questions() throws -> [QuestionAnswer] {
    guard let url = Bundle(for: DataProvider.self).url(forResource: "Quiz", withExtension: "json") else {
      throw JSONError.invalidURL
    }
    
    do {
      let data = try Data(contentsOf: url)
      
      let decoder = JSONDecoder()
      let quiz = try decoder.decode(Quiz.self, from: data)
      
      if quiz.questions.isEmpty {
        throw JSONError.emptyData
      }
      
      return quiz.questions
    } catch {
      throw error
    }
  }
}
